#!/bin/bash
yum update -y
# install necessary files for docker and docker-compose
yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce
yum install -y docker-compose
tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://2rou9iap.mirror.aliyuncs.com"]
}
EOF

systemctl start docker
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# install wordpress startup project
yum install -y git
git clone https://gitlab.com/flyingtimes/wordpress-startup.git

