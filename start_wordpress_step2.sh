#!/bin/bash
source .env
echo "using ${SUBDOMAIN}.${DOMAIN} as your website url. Please change it in .env file if necessary."
sed -i "s/\(server_name \)\(.*\)/\1${SUBDOMAIN}.${DOMAIN};/" ./etc/nginx/app.conf
sed -i "s/\(server_name \)\(.*\)/\1${SUBDOMAIN}.${DOMAIN};/" ./etc/nginx/app-ssl.conf
docker-compose up -d